package ru.t1.nikitushkina.tm.service;

import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.nikitushkina.tm.api.service.IPropertyService;
import ru.t1.nikitushkina.tm.marker.UnitCategory;

@Category(UnitCategory.class)
public final class PropertyServiceTest {

    @Nullable
    private static IPropertyService PROPERTY_SERVICE;

    @Test
    public void getApplicationVersion() {
        Assert.assertNotNull(PROPERTY_SERVICE.getApplicationVersion());
    }

    @Test
    public void getAuthorEmail() {
        Assert.assertNotNull(PROPERTY_SERVICE.getAuthorEmail());
    }

    @Test
    public void getAuthorName() {
        Assert.assertNotNull(PROPERTY_SERVICE.getAuthorName());
    }

    @Test
    public void getServerHost() {
        Assert.assertNotNull(PROPERTY_SERVICE.getServerHost());
    }

    @Test
    public void getServerPort() {
        Assert.assertNotNull(PROPERTY_SERVICE.getServerPort());
    }

    @Test
    public void getSessionKey() {
        Assert.assertNotNull(PROPERTY_SERVICE.getSessionKey());
    }

    @Test
    public void getSessionTimeout() {
        Assert.assertNotNull(PROPERTY_SERVICE.getSessionTimeout());
    }

}
