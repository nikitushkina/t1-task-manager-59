package ru.t1.nikitushkina.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.nikitushkina.tm.api.repository.dto.IDTORepository;
import ru.t1.nikitushkina.tm.api.service.dto.IDTOService;
import ru.t1.nikitushkina.tm.dto.model.AbstractModelDTO;
import ru.t1.nikitushkina.tm.enumerated.Sort;
import ru.t1.nikitushkina.tm.exception.field.IdEmptyException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Comparator;
import java.util.List;

@Service
@NoArgsConstructor
public abstract class AbstractDTOService<M extends AbstractModelDTO, R extends IDTORepository<M>> implements IDTOService<M> {

    @Getter
    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    @Nullable
    protected abstract IDTORepository<M> getRepository();

    @NotNull
    @Override
    @Transactional
    public M add(@NotNull final M model) {
        getRepository().add(model);
        return model;
    }

    @Override
    @Transactional
    public void clear() {
        getRepository().clear();
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return getRepository().existsById(id);
    }

    @Nullable
    @Override
    public List<M> findAll() {
        return getRepository().findAll();
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final Comparator comparator) {
        if (comparator == null) return findAll();
        return getRepository().findAll(comparator);
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        return getRepository().findAll(sort.getComparator());
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return getRepository().findOneById(id);
    }

    @Override
    public int getSize() {
        return getRepository().getSize();
    }

    @Override
    @Transactional
    public void remove(@Nullable final M model) {
        if (model == null) return;
        getRepository().remove(model);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        @Nullable M result = findOneById(id);
        remove(result);
    }

    @Override
    @Transactional
    public void update(@Nullable final M model) {
        if (model == null) return;
        getRepository().update(model);
    }

}
