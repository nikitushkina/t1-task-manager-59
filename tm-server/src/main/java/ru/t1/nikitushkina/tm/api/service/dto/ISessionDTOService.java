package ru.t1.nikitushkina.tm.api.service.dto;

import ru.t1.nikitushkina.tm.dto.model.SessionDTO;

public interface ISessionDTOService extends IUserOwnedDTOService<SessionDTO> {
}
