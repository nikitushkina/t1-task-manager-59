package ru.t1.nikitushkina.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.nikitushkina.tm.dto.model.SessionDTO;
import ru.t1.nikitushkina.tm.dto.model.UserDTO;

public interface IAuthService {

    @NotNull
    String login(@Nullable String login, @Nullable String password) throws Exception;

    void logout(@Nullable SessionDTO session) throws Exception;

    @NotNull
    UserDTO registry(@Nullable String login,
                     @Nullable String password,
                     @Nullable String email,
                     @Nullable String fstName,
                     @Nullable String lstName,
                     @Nullable String mdlName
    ) throws Exception;

    @NotNull
    SessionDTO validateToken(@Nullable String token) throws Exception;

}
