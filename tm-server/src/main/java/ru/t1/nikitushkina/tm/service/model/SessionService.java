package ru.t1.nikitushkina.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.nikitushkina.tm.api.repository.model.ISessionRepository;
import ru.t1.nikitushkina.tm.api.service.model.ISessionService;
import ru.t1.nikitushkina.tm.model.Session;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    @Nullable
    @Autowired
    private ISessionRepository repository;

    @Nullable
    protected ISessionRepository getRepository() {
        return repository;
    }

}
