package ru.t1.nikitushkina.tm.log;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE;

}
