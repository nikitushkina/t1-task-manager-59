package ru.t1.nikitushkina.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.nikitushkina.tm.api.endpoint.IAdminEndpoint;
import ru.t1.nikitushkina.tm.api.service.IAdminService;
import ru.t1.nikitushkina.tm.dto.request.SchemeDropRequest;
import ru.t1.nikitushkina.tm.dto.request.SchemeInitRequest;
import ru.t1.nikitushkina.tm.dto.response.SchemeDropResponse;
import ru.t1.nikitushkina.tm.dto.response.SchemeInitResponse;
import ru.t1.nikitushkina.tm.exception.EndpointException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@NoArgsConstructor
@AllArgsConstructor
@WebService(endpointInterface = "ru.t1.nikitushkina.tm.api.endpoint.IAdminEndpoint")
public class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    @NotNull
    @Autowired
    private IAdminService adminService;

    @WebMethod
    @Override
    @SneakyThrows
    public SchemeDropResponse dropScheme(
            @WebParam(name = REQUEST, partName = REQUEST)
            @Nullable final SchemeDropRequest request
    ) {
        try {
            adminService.dropScheme(request.getInitToken());
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new SchemeDropResponse();
    }

    @WebMethod
    @Override
    @SneakyThrows
    public SchemeInitResponse initScheme(
            @WebParam(name = REQUEST, partName = REQUEST)
            @Nullable final SchemeInitRequest request
    ) {
        try {
            adminService.initScheme(request.getInitToken());
        } catch (@NotNull final Exception e) {
            throw new EndpointException(e.getMessage());
        }
        return new SchemeInitResponse();
    }

}
