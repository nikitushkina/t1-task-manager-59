package ru.t1.nikitushkina.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.nikitushkina.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

    @NotNull
    public static final String APPLICATION_VERSION_KEY = "buildNumber";
    @NotNull
    public static final String AUTHOR_EMAIL_KEY = "email";
    @NotNull
    public static final String AUTHOR_NAME_KEY = "developer";
    @NotNull
    public static final String GIT_BRANCH = "gitBranch";
    @NotNull
    public static final String GIT_COMMIT_ID = "gitCommitId";
    @NotNull
    public static final String GIT_COMMIT_TIME = "gitCommitTime";
    @NotNull
    public static final String GIT_COMMIT_MSG_FULL = "gitCommitMsgFull";
    @NotNull
    public static final String GIT_COMMITTER_NAME = "gitCommitterName";
    @NotNull
    public static final String GIT_COMMITTER_EMAIL = "gitCommitterEmail";

    @Value("#{environment['server.port']}")
    private String port;
    @Value("#{environment['server.host']}")
    private String host;
    @Value("#{environment['admin.login']}")
    private String adminLogin;
    @Value("#{environment['admin.password']}")
    private String adminPassword;
    @Value("#{environment['empty.value']}")
    private String emptyValue;

    @NotNull
    @Override
    public String getApplicationVersion() {
        return read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getCommitId() {
        return read(GIT_COMMIT_ID);
    }

    @NotNull
    @Override
    public String getCommitMsgFull() {
        return read(GIT_COMMIT_MSG_FULL);
    }

    @NotNull
    @Override
    public String getCommitTime() {
        return read(GIT_COMMIT_TIME);
    }

    @NotNull
    @Override
    public String getCommitterEmail() {
        return read(GIT_COMMITTER_EMAIL);
    }

    @NotNull
    @Override
    public String getCommitterName() {
        return read(GIT_COMMITTER_NAME);
    }

    @NotNull
    @Override
    public String getGitBranch() {
        return read(GIT_BRANCH);
    }

    @NotNull
    private String read(@Nullable final String key) {
        if (key == null || key.isEmpty()) return emptyValue;
        if (!Manifests.exists(key)) return emptyValue;
        return Manifests.read(key);
    }

}
