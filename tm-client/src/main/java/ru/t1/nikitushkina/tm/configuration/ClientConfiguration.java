package ru.t1.nikitushkina.tm.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.t1.nikitushkina.tm.api.endpoint.*;
import ru.t1.nikitushkina.tm.api.service.IPropertyService;

@Configuration
@ComponentScan("ru.t1.nikitushkina.tm")
public class ClientConfiguration {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Bean
    @NotNull
    public IAuthEndpoint getAuthEndpoint() {
        return IAuthEndpoint.newInstance();
    }

    @Bean
    @NotNull
    public IProjectEndpoint getProjectEndpoint() {
        return IProjectEndpoint.newInstance();
    }

    @Bean
    @NotNull
    public ISystemEndpoint getSystemEndpoint() {
        return ISystemEndpoint.newInstance();
    }

    @Bean
    @NotNull
    public ITaskEndpoint getTaskEndpoint() {
        return ITaskEndpoint.newInstance();
    }

    @Bean
    @NotNull
    public IUserEndpoint getUserEndpoint() {
        return IUserEndpoint.newInstance();
    }

}
