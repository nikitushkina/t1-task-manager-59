package ru.t1.nikitushkina.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.nikitushkina.tm.api.endpoint.ISystemEndpoint;
import ru.t1.nikitushkina.tm.api.service.IPropertyService;
import ru.t1.nikitushkina.tm.enumerated.Role;
import ru.t1.nikitushkina.tm.listener.AbstractListener;

@Component
public abstract class AbstractSystemListener extends AbstractListener {

    @NotNull
    @Autowired
    protected IPropertyService propertyService;

    @Nullable
    @Autowired
    protected AbstractListener[] listeners;

    @NotNull
    @Autowired
    protected ISystemEndpoint systemEndpoint;

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
