package ru.t1.nikitushkina.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.nikitushkina.tm.dto.request.ProjectCompleteByIdRequest;
import ru.t1.nikitushkina.tm.event.ConsoleEvent;
import ru.t1.nikitushkina.tm.util.TerminalUtil;

@Component
public final class ProjectCompleteByIdListener extends AbstractProjectListener {

    @NotNull
    public static final String NAME = "project-complete-by-id";

    @NotNull
    public static final String DESCRIPTION = "Complete project by id.";

    @Override
    @EventListener(condition = "@projectCompleteByIdListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.print("ENTER ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @Nullable final ProjectCompleteByIdRequest request = new ProjectCompleteByIdRequest(getToken());
        request.setProjectId(id);
        projectEndpoint.completeById(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
