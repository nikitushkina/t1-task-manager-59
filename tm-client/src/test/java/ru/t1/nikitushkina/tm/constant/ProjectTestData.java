package ru.t1.nikitushkina.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

@UtilityClass
public final class ProjectTestData {

    @NotNull
    public final static String USER_PROJECT1_NAME = "Project 01";

    @NotNull
    public final static String USER_PROJECT1_DESCRIPTION = "Description 01";

    @NotNull
    public final static String USER_PROJECT2_NAME = "Project 02";

    @NotNull
    public final static String USER_PROJECT2_DESCRIPTION = "Description 02";

    @NotNull
    public final static String USER_PROJECT3_NAME = "Project 03";

    @NotNull
    public final static String USER_PROJECT3_DESCRIPTION = "Description 03";

}
